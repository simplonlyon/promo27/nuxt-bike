export interface Station {
    id?:string;
    name:string;
    latitude:number;
    longitude:number;
    slots:number
    availables?:number;
}

export interface Bike {
    id?:string;
    electric:boolean;
    battery:number;
    station?:Station;
}

export interface Rent {
    id?:string;
    startDate:string;
    endDate?:string;
    startStation:Station;
    endStation?:Station;
    bike:Bike;
    cost?:number;
}